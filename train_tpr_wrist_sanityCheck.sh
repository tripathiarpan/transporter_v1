#!/bin/bash
#SBATCH --nodes 1  
#SBATCH --gres=gpu:v100l:1
#SBATCH --cpus-per-task=12    
#SBATCH --mem=16G  
#SBATCH --time=00-04:00
#SBATCH --output=%N-%j.out

module load python/3.6
#virtualenv --no-download ~/ENV
source ~/ENV/bin/activate
#pip install torchvision av pytorch-lightning --no-index


export MASTER_ADDR=$(hostname) #Store the master node’s IP address in the MASTER_ADDR environment variable.

echo "r$SLURM_NODEID master: $MASTER_ADDR"

echo "r$SLURM_NODEID Launching python script"

# The SLURM_NTASKS variable tells the script how many processes are available for this execution. “srun” executes the script <tasks-per-node * nodes> times

python -u train_lus_transporter_wrist_radon.py --num_gpus 1 --batch_size 16 --num_workers 12 --lr 1e-4 --max_epochs 60 --sample_rate 4 --name 'lr0.0001_idxcsv_dptdecay_samrate=4_radonv1.2'